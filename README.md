# Project showing how to perform unit testing with GoogleTest and CMake and Gcov, and test coverage in GitLab CI/CD

![Test Coverage report for C++ and GoogleTest, tests hit](docs/images/gitlab_test_coverage_googletest_cpp_cmake_gcov_tests_hit.png)

This is a modified fork from [this project](https://github.com/QianYizhou/gtest-cmake-gcov-example) which itself is forked from [here](https://github.com/kaizouman/gtest-cmake-example) being the source for the article [Unit testing with GoogleTest and CMake](http://www.kaizou.org/2014/11/gtest-cmake.html).

Helping answer a GitLab community question, details in [this forum topic](https://forum.gitlab.com/t/local-machine-to-gitlab-code-coverage-using-gcov-lcov-and-gtest/80081).

## Pre-requisites

Clone this project and initialize the Git submodule for `gtest/googletest`. You can also clone with the `--recursive` paramter.

```shell 
git clone https://gitlab.com/gitlab-de/use-cases/coverage-reports/gitlab-test-coverage-googletest-cmake-gcov.git

cd gitlab-test-coverage-googletest-cmake-gcov

git submodule init
git submodule update
```

OR

```shell
git clone --recursive https://gitlab.com/gitlab-de/use-cases/coverage-reports/gitlab-test-coverage-googletest-cmake-gcov.git

cd gitlab-test-coverage-googletest-cmake-gcov
```

Install a C++ compiler (g++, clang++), CMake, Make, Git, rcov, lcov.

Example on macOS with Homebrew:

```
brew install git cmake make rcovr lcov
```

## Build

Create a new build directory and configure CMake. Invoke `make` to the build the source.

```
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug -Dtest=ON
make
cd ..
```

## Run Tests

Use the special make targets for `coverage` tests and Cobertura XML report files.

```
cd build

make test

make coverage_TESTNAME // Check the coverage, print in foreground

make coverage_cobertura_TESTNAME // Check coverage, write to Cobertura XML report

```

This can be run outside of the build directory using `-C build` as paramter at the end. 

```
make test -C build
make coverage_TESTNAME -C build
make coverage_cobertura_TESTNAME -C build
```

or

```
build/test/testfoo/testFoo
```

### GitLab CI/CD Integration with Cobertura XML reports

GitLab supports Cobertura XML reports to [visualize test coverage](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html#cc-example). 

This project provides a [CMake helper function](cmake/CodeCoverage.cmake) that is used in [tests/testFoo/CMakeLists.txt](tests/testFoo/CMakeLists.txt) to provide the make target `coverage_cobertura_testFoo`.


```
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug .. -Dtest=ON
make coverage_cobertura_testFoo
```

The Cobertura XML report is written to `build/coverage_cobertura_testFoo.xml`.

Check the [.gitlab-ci.yml](.gitlab-ci.yml) file for more details. 

An example can be found in [this MR](https://gitlab.com/dnsmichi/gtest-cmake-gcov-example/-/merge_requests/1).

![Test Coverage report for C++ and GoogleTest, zero tests](docs/images/gitlab_test_coverage_googletest_cpp_cmake_gcov_tests_zero.png)



## Editing

### Add new tests

Create a new directory in the `test` directory. Follow the example structur from `testfoo`.

```
$ mkdir -p test/mynewtest

$ vim test/CMakeLists.txt

# Include the new test sub directory
add_subdirectory(mynewtest)

```

`test/mynewtest/CMakeLists.txt` needs to include the test files/libs, define the `BIN_NAME` and setup the Coverage Cobertura reports.

```

SETUP_TARGET_FOR_COVERAGE_COBERTURA(
    coverage_cobertura_${BIN_NAME}  # Name for custom target.
    ${BIN_NAME}         # Name of the test driver executable that runs the tests.
    # NOTE! This should always have a ZERO as exit code
    # otherwise the coverage generation will not complete.
    coverage_cobertura_${BIN_NAME}            # Name of output file.
    )

```

Update the CMake cache, and run the new test coverage make targets.

```
cd build

cmake .. -DCMAKE_BUILD_TYPE=Debug -Dtest=ON

make coverage_cobertura_mynewtest
```

## Thanks

Thanks to
1. https://github.com/pyarmak/cmake-gtest-coverage-example.git
2. https://github.com/kaizouman/gtest-cmake-example.git
3. https://github.com/dmonopoly/gtest-cmake-example.git

